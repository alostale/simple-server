package org.alo.srv;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

public abstract class LoggingHandler implements HttpHandler {
  private static int cnt = 0;
  protected boolean logBody = true;
  protected boolean logHeaders = true;

  protected abstract String doHandle(String body, HttpExchange xchg);

  @Override
  public void handle(HttpExchange xchg) throws IOException {
    cnt += 1;
    Headers headers = xchg.getRequestHeaders();
    Set<Map.Entry<String, List<String>>> entries = headers.entrySet();
    String uri = xchg.getRequestURI().toString();

    System.out.println(new Date() + " [" + cnt + "] " + xchg.getRequestMethod() + " " + uri);
    if (logHeaders) {
      entries.forEach(e -> System.out.println(e.getKey() + ":" + e.getValue()));
    }

    InputStream b = xchg.getRequestBody();

    String body = new String(b.readAllBytes());
    if (logBody) {
      System.out.println(body);
    }
    String resp = doHandle(body, xchg);

    /*
     * StringBuffer response = new StringBuffer(); for (Map.Entry<String, List<String>> entry :
     * entries) { response.append(entry.toString() + "\n"); }
     */

    xchg.sendResponseHeaders(body.equals("fail") ? 500 : 200, resp.length());
    OutputStream os = xchg.getResponseBody();
    os.write(resp.getBytes());
    os.close();

  }

  protected Map<String, String> getQueryParams(HttpExchange xchg) {
    String uri = xchg.getRequestURI().toString();
    Map<String, String> queryParams = new HashMap<>();
    if (uri.contains("?")) {
      String allParams = uri.substring(uri.indexOf("?") + 1);
      for (String ps : allParams.split("&")) {
        String[] p = ps.split("=");
        System.out.println(p);
        queryParams.put(p[0], p[1]);
      }
    }
    return queryParams;
  }

}
