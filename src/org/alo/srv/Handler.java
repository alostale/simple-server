package org.alo.srv;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class Handler extends LoggingHandler {
  private static int cnt = 0;

  @Override
  public void handle(HttpExchange xchg) throws IOException {
    cnt += 1;
    Headers headers = xchg.getRequestHeaders();
    Set<Map.Entry<String, List<String>>> entries = headers.entrySet();
    URI uri = xchg.getRequestURI();
    System.out.println(new Date() + " [" + cnt + "] " + xchg.getRequestMethod() + " " + uri);
    InputStream b = xchg.getRequestBody();

    String body = new String(b.readAllBytes());
    System.out.println(body);

    String response = "resp...";

    xchg.sendResponseHeaders(body.equals("fail") ? 500 : 200, response.length());
    OutputStream os = xchg.getResponseBody();
    os.write(response.getBytes());
    os.close();

  }

  @Override
  protected String doHandle(String body, HttpExchange xchg) {
    return "";

  }

}
