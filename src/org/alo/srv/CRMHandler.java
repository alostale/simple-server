package org.alo.srv;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Iterator;
import java.util.Optional;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;

public class CRMHandler extends LoggingHandler {
  private CustomerStore customers = new CustomerStore();

  @Override
  protected String doHandle(String body, HttpExchange xchg) {
    setCORS(xchg);

    if ("ping".equals(getQueryParams(xchg).get("action"))) {
      return "1";
    }

    if ("".equals(body)) {
      return "";
    }
    try {
      JSONObject rq = new JSONObject(body);
      String action = rq.getString("action");

      switch (action) {
        case "createNewBP_Object":
          JSONObject cust = rq.getJSONObject("params");
          return createNew(cust).toString();
        case "searchByName_Object":
          // For now return them all
          return customers.toString();
        case "searchByID_Object":
          return byId(rq.getJSONObject("params").getString("xID")).orElse(new JSONObject())
              .toString();
        case "updateBP_Object":
          return update(rq.getJSONObject("params"));
        default:
          throw new IllegalArgumentException("Unsupported action: " + action);
      }
    } catch (JSONException e) {
      throw new RuntimeException(e);
    }

  }

  private JSONObject createNew(JSONObject cust) throws JSONException {
    String id = cust.optString("xID");
    if (id == null || "".equals(id) || "null".equals(id)) {
      cust.put("xID", Long.toString(System.currentTimeMillis()));
    }
    customers.put(cust);
    return cust;
  }

  private String update(JSONObject custUpdated) throws JSONException {
    String id = custUpdated.optString("xID");
    JSONObject cust = byId(id).orElseGet(() -> {
      customers.put(custUpdated);
      return custUpdated;
    });

    @SuppressWarnings("unchecked")
    Iterator<String> keys = custUpdated.keys();
    while (keys.hasNext()) {
      String key = keys.next();
      cust.put(key, custUpdated.get(key));
    }
    customers.flush();
    return cust.toString();
  }

  private Optional<JSONObject> byId(String id) throws JSONException {
    for (int i = 0; i < customers.length(); i++) {
      JSONObject cust = customers.getJSONObject(i);
      if (id.equals(cust.optString("xID"))) {
        return Optional.of(cust);
      }
    }
    return Optional.empty();
  }

  private void setCORS(HttpExchange xchg) {
    Headers respHeaders = xchg.getResponseHeaders();
    respHeaders.add("Access-Control-Allow-Origin", "*");
    respHeaders.add("Access-Control-Allow-Credentials", "true");
    respHeaders.add("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS");
    respHeaders.add("Access-Control-Allow-Headers",
        "Content-Type, Origin, Accept, X-Requested-With, Access-Control-Allow-Credentials, Authorization");

    respHeaders.add("Access-Control-Max-Age", "10000");
  }

  private static class CustomerStore {
    private Path store = Paths.get(System.getProperty("user.home"), ".sample-crm.json");
    private JSONArray customers = new JSONArray();

    public CustomerStore() {
      if (Files.exists(store)) {
        try {
          customers = new JSONArray(Files.readString(store));
        } catch (JSONException | IOException e) {
          e.printStackTrace();
        }
      }

    }

    public void put(JSONObject cust) {
      customers.put(cust);
      flush();
    }

    private void flush() {
      try {
        Files.write(store, customers.toString().getBytes(), StandardOpenOption.CREATE);
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    public JSONObject getJSONObject(int i) {
      try {
        return customers.getJSONObject(i);
      } catch (JSONException e) {
        throw new RuntimeException(e);
      }
    }

    public int length() {
      return customers.length();
    }

    @Override
    public String toString() {
      return customers.toString();
    }

  }

}
