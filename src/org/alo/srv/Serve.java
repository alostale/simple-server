package org.alo.srv;

import java.io.IOException;
import java.net.InetSocketAddress;

import com.sun.net.httpserver.HttpServer;

public class Serve {
  private static final int PORT = 8000;

  public static void main(String[] args) throws IOException {

    HttpServer server = HttpServer.create(new InetSocketAddress("localhost", PORT), 0);

    server.createContext("/", new Handler());
    server.createContext("/incoming", new Handler());
    server.createContext("/auth", new AuthHandler());
    server.createContext("/crm", new CRMHandler());
    System.out.println("Serving at " + PORT);
    long idleInterval = Long.getLong("sun.net.httpserver.idleInterval", 30);
    System.out.println("Connection idle interval (sec): " + idleInterval);

    server.start();
  }

}
